﻿using OpenTK.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INFOGR2023Template
{
    public class Light
    {
        public Vector3 position;
        public float r;
        public float g;
        public float b;

        public Light(Vector3 position, float r, float g, float b)
        {
            this.position = position;
            this.r = r;
            this.g = g;
            this.b = b;
        }
    }
}
