﻿using OpenTK.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INFOGR2023Template
{
    public class Sphere
    {
        public Vector3 center;
        public float radius;
        public Color4 color;

        public Sphere(Vector3 center, float radius, Color4 color)
        {
            this.center = center;
            this.radius = radius;
            this.color = color;
        }
    }

    public class Plane
    {
        public Vector3 normal;
        public float distanceOrigin;
        public Color4 color;

        public Plane(Vector3 normal, float distanceOrigin, Color4 color)
        {
            this.normal = normal;
            this.distanceOrigin = distanceOrigin;
            this.color = color;
        }
    }
}
