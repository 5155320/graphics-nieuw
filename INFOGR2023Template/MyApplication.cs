using INFOGR2023Template;
using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using System;

namespace Template
{
    class MyApplication
    {
        // member variables
        public Surface screen;
        // constructor
        public MyApplication(Surface screen)
        {
            this.screen = screen;
        }
        // initialize
        public void Init()
        {
            /// Define camera parameters
            Vector3 cameraPosition = new Vector3(0.0f, 0.0f, 0.0f);
            Vector3 cameraLookAt = new Vector3(0.0f, 0.0f, 1.0f);
            Vector3 cameraUp = new Vector3(0.0f, 1.0f, 0.0f);
            Vector3 cameraRight = new Vector3(1.0f, 0.0f, 0.0f);
            float d = 60;
            int aspectRatio = 16 / 9;

            // Define plane
            //Vector3 planeNormal = -cameraLookAt;
            //Vector3 planePoint = cameraPosition + cameraLookAt;

            Vector3 planeCenter = cameraPosition + d * cameraLookAt;
            Vector3 corner1 = planeCenter + cameraUp - aspectRatio * cameraRight;
            Vector3 corner2 = planeCenter + cameraUp + aspectRatio * cameraRight;
            Vector3 corner3 = planeCenter - cameraUp - aspectRatio * cameraRight;
            Vector3 corner4 = planeCenter - cameraUp + aspectRatio * cameraRight;

            Vector3 u = corner1 - corner2;
            Vector3 v = corner2 - corner3;

            Vector3 light = new Vector3(10, 200, 0);
            Vector3 lightColor = new Vector3(1, 1, 1);

            // Calculate half width and height of the view plane
            float halfWidth = (float)Math.Tan(MathHelper.DegreesToRadians(d) / 2);
            float halfHeight = halfWidth * screen.height / screen.width;

            Plane redPlane = new Plane(new Vector3(0, 0, 1), 5, Color4.Red);
            Sphere greenSphere = new Sphere(new Vector3(200, 200, 500), 100, Color4.Green);
            Sphere blueSphere = new Sphere(new Vector3(400, 230, 400), 70, Color4.Blue);
            
            // Define plane parameters
            Vector3 planePoint = new Vector3(0, 0, 0); // A point on the plane
            Vector3 planeNormal = new Vector3(1, 1, 1); // Normal vector of the plane (e.g., pointing upwards)

            // Calculate plane coefficients
            float planeD = -Vector3.Dot(planeNormal, planePoint);
            Vector4 planeCoefficients = new Vector4(planeNormal.X, planeNormal.Y, planeNormal.Z, planeD);


            for (int x = 0; x < screen.width; x++)
            {
                for (int y = 0; y < screen.height; y++)
                {

                    // Calculate ray direction
                    //float offsetX = (x - screen.width / 2f + 0.5f) * halfWidth * 2f;
                    //float offsetY = (y - screen.height / 2f + 0.5f) * halfHeight * 2f;

                    //Vector3 rayDirection = Vector3.Normalize(cameraDirection + offsetX * cameraRight + offsetY * cameraActualUp);
                    // Calculate ray origin
                    //Vector3 rayOrigin = cameraPosition;

                    Vector3 rayOrigin = new Vector3(x, y, 0);
                    Vector3 rayDirection = new Vector3(0, 0, 1);

                    Vector3 shadowRay = light - rayOrigin;

                    // Check intersections
                    float t1 = Intersection.SphereIntersection(rayOrigin, rayDirection, greenSphere.center, greenSphere.radius);
                    float t2 = Intersection.SphereIntersection(rayOrigin, rayDirection, blueSphere.center, blueSphere.radius);
                    float t3 = Intersection.PlaneIntersection(rayOrigin, rayDirection, redPlane);

                    /*Console.WriteLine(redPlane.normal);
                    Console.WriteLine(redPlane.distanceOrigin);
                    Console.WriteLine(rayOrigin);
                    Console.WriteLine(t3);
                    break;*/

                    if (t3 > 200 && t3 < screen.width)
                    {
                        screen.Plot(x, y, Color4.Red.ToArgb());
                    }

                    if (t1 > 0)
                    {
                        float epsilon = 0.000001f;
                        Vector3 intersectionPoint = rayOrigin + rayDirection * t1;

                        if (Vector3.Distance(intersectionPoint, greenSphere.center) < epsilon)
                        {
                            continue;
                        }

                        /*Vector3 normal = Vector3.Normalize(intersectionPoint - sphere1Center);
                        float intensity = Vector3.Dot(normal, Vector3.Normalize(light - intersectionPoint) * 0.8f);
                        Vector3 l = new Vector3(light - intersectionPoint);
                        float nTimesl = normal.Z * l.Z;
                        float pixelColor = intensity * Math.Max(0, nTimesl);
                        Vector3 shadedColor = new Vector3(sphere1Color.ToArgb() * intensity, sphere1Color.ToArgb() * intensity, sphere1Color.ToArgb() * intensity);
                        screen.Plot(x, y, new Color4(pixelColor, pixelColor, pixelColor * Color4.Red.ToArgb(), 1).ToArgb());*/

                        Vector3 lightRay = (light - intersectionPoint) / (-light + intersectionPoint);
                        //Random rand = new Random()
                        //int t = Random(0, light - intersectionPoint);
                        Vector3 shadowRays = intersectionPoint + lightRay;
                        int color = Color4.Green.ToArgb() * Convert.ToInt32(shadowRay.X);
                        screen.Plot(x, y, new Color4(0, 0, color, 1).ToArgb());
                    }
                    if (t2 > 0)
                    {
                        Vector3 intersectionPoint = rayOrigin + rayDirection * t1;
                        Vector3 lightRay = (light - intersectionPoint) / (-light + intersectionPoint);
                        //Random rand = new Random()
                        //int t = Random(0, light - intersectionPoint);
                        Vector3 shadowRays = intersectionPoint + lightRay;
                        int color = Color4.Blue.ToArgb() * Convert.ToInt32(shadowRay.X);
                        screen.Plot(x, y, new Color4(0, color, color, 1).ToArgb());



                    }

                }
            }


        }

        // tick: renders one frame
        public void Tick()
        {

        }
    }
}