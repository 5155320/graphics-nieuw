﻿using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using SixLabors.ImageSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template;

namespace INFOGR2023Template
{
    public class Camera
    {
        public Vector3 position;
        public Vector3 lookAtDirection;
        public Vector3 upDirection;
        public List<Vector3> screenPlaneCorners;

        public Camera(Surface screen)
        {
            position = new Vector3(0, 0, 0);
            lookAtDirection = new Vector3(0, 0, 1);
            upDirection = new Vector3(0, 1, 0);
            List<Vector3> corners = new List<Vector3>();
            corners.Add(new Vector3(screen.width/2, screen.height/2, 0));
            corners.Add(new Vector3(-screen.width / 2, screen.height / 2, 0));
            corners.Add(new Vector3(screen.width / 2, -screen.height / 2, 0));
            corners.Add(new Vector3(-screen.width / 2, -screen.height / 2, 0));
            screenPlaneCorners = corners;
        }
    }
}
