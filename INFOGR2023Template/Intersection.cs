﻿using OpenTK.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INFOGR2023Template
{
    public class Intersection
    {
        public static float SphereIntersection(Vector3 rayOrigin, Vector3 rayDirection, Vector3 sphereCenter, float sphereRadius)
        {
            Vector3 oc = rayOrigin - sphereCenter;
            float a = Vector3.Dot(rayDirection, rayDirection);
            float b = 2 * Vector3.Dot(oc, rayDirection);
            float c = Vector3.Dot(oc, oc) - sphereRadius * sphereRadius;
            float discriminant = b * b - 4 * a * c;

            if (discriminant < 0)
                return -1; // No intersection

            float t1 = (-b + (float)Math.Sqrt(discriminant)) / (2 * a);
            float t2 = (-b - (float)Math.Sqrt(discriminant)) / (2 * a);

            if (t1 > 0 && t2 > 0)
                return Math.Min(t1, t2); // Smallest positive intersection
            return -1;
        }

        public static float PlaneIntersection(Vector3 rayOrigin, Vector3 rayDirection, Plane plane)
        {

        }

        public float IntersectPlane1(Vector3 rayOrigin, Vector3 rayDirection, Vector4 planeCoefficients)
        {
            float epsilon = 0.0001f; // A small value to account for floating-point errors

            // Calculate the dot product between the ray direction and the plane normal
            float dotProduct = Vector3.Dot(rayDirection, new Vector3(planeCoefficients.X, planeCoefficients.Y, planeCoefficients.Z));

            // Check if the ray is parallel or almost parallel to the plane
            if (Math.Abs(dotProduct) < epsilon)
            {
                // Ray is parallel to the plane, no intersection
                return -1f;
            }

            // Calculate the distance from the ray origin to the plane
            Vector3 rayToPlane = rayOrigin - planeCoefficients.W * new Vector3(planeCoefficients.X, planeCoefficients.Y, planeCoefficients.Z);
            float distance = Vector3.Dot(rayToPlane, new Vector3(planeCoefficients.X, planeCoefficients.Y, planeCoefficients.Z)) / dotProduct;

            // Check if the intersection is behind the ray origin
            if (distance < 0)
            {
                // Intersection is behind the ray origin, discard it
                return -1f;
            }

            return distance;
        }

        public static float PlaneIntersection1(Vector3 rayOrigin, Vector3 rayDirection, Vector3 planePoint, Vector3 planeNormal)
        {
            float epsilon = 0.0001f; // A small value to account for floating-point errors

            // Calculate the dot product between the ray direction and the plane normal
            float dotProduct = Vector3.Dot(rayDirection, planeNormal);

            if (dotProduct == 0)
            {
                return -1;
            }

            // Check if the ray is parallel or almost parallel to the plane
            if (Math.Abs(dotProduct) < epsilon)
            {
                // Ray is parallel to the plane, no intersection
                return -1f;
            }

            // Calculate the distance from the ray origin to the plane
            Vector3 rayToPlane = planePoint - rayOrigin;
            float distance = Vector3.Dot(rayToPlane, planeNormal) / dotProduct;

            // Check if the intersection is behind the ray origin
            if (distance < 0f)
            {
                // Intersection is behind the ray origin, no intersection
                return -1f;
            }

            // Return the distance to the intersection point
            return 1;
        }
    }
}
